package com.gitgudstudios.zerocontact.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.gitgudstudios.zerocontact.ZeroContact;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "ZeroContact";
		config.width = 800;
		config.height = 480;
		new LwjglApplication(new ZeroContact(), config);
	}
}
