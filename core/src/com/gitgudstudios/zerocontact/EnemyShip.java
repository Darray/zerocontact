package com.gitgudstudios.zerocontact;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public class EnemyShip{
    public static final int SPEED = 150;
    public static final int DEFAULT_Y = 480;
    private static Texture enemyShipImage;
    Rectangle hitBox;
    float x, y;
    public Boolean isDead;

    public EnemyShip(float originX, Texture tex){
        this.x = originX;
        this.y = DEFAULT_Y;
        this.isDead = false;
        this.hitBox = new Rectangle(x, y, 64, 64);
        this.enemyShipImage = tex;

    }
    public void update(float deltaTime){
        y -= SPEED * deltaTime;
        hitBox.y = y;
        if (y < -64){
            isDead = true;
        }
    }
    public void render(SpriteBatch batch){
        if (!isDead){
            batch.draw(enemyShipImage, x, y);
        }
    }
}
