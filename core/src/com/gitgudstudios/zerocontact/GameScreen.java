package com.gitgudstudios.zerocontact;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.TimeUtils;

import java.util.ArrayList;

public class GameScreen implements Screen {
    final ZeroContact game;

    Texture shipImage;
    Texture bulletTexture;
    Texture enemyShipTexture;
    Sound bulletSound;
    Music backgroundMusic;
    OrthographicCamera camera;
    Rectangle ship;
    String scoreString = new String("Ships Destroyed: 0");
    private final ArrayList<EnemyShip> activeEnemyShips = new ArrayList<>();
    private final ArrayList<EnemyShip> deadShips = new ArrayList<>();
    private final ArrayList<Bullet> activeBullets = new ArrayList<>();
    private final ArrayList<Bullet> deadBullets = new ArrayList<>();
    long lastBulletTime;
    long lastEnemySpawnTime;
    int shipsDestroyed;

    public GameScreen(final ZeroContact gam) {
        this.game = gam;
        //TODO place all static assets in one bitmap to reduce render time
        //TODO implement AssetLoader
        // load the images for the spaceship and bullet
        shipImage = new Texture(Gdx.files.internal("spaceship00.png"));
        bulletTexture = new Texture(Gdx.files.internal("bullet.png"));
        enemyShipTexture = new Texture(Gdx.files.internal("enemyship00.png"));

        // load the bullet sound effect and the background music
        bulletSound = Gdx.audio.newSound(Gdx.files.internal("pew.wav"));
        backgroundMusic = Gdx.audio.newMusic(Gdx.files.internal("gamemusic.ogg"));
        backgroundMusic.setLooping(true);

        // create the camera and the SpriteBatch
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800, 480);

        // create a Rectangle to logically represent the ship
        // TODO shouldn't the object's hitbox belong to itself and not the GameScreen class??
        ship = new Rectangle();
        ship.x = 800 / 2 - 64 / 2; // center the ship horizontally
        ship.y = 20; // bottom left corner of the ship is 20 pixels above
        // the bottom screen edge
        ship.width = 64;
        ship.height = 64;
    }
    private void spawnBullets(){
            lastBulletTime = TimeUtils.nanoTime(); // this should probably be tied to DeltaTime, js
            Bullet newBullet = new Bullet(ship.x + 28, ship.y + 64, bulletTexture);
            activeBullets.add(newBullet);
            bulletSound.play();
    }
    private void updateBullets(){
        //update active bullets
        for (Bullet bullet : activeBullets) {
            bullet.update(Gdx.graphics.getDeltaTime());
            if (bullet.isDead) {
                deadBullets.add(bullet);
            }
            // there has to be a better way to implement collision
            for (EnemyShip enemyShip : activeEnemyShips){
                if (bullet.hitBox.overlaps(enemyShip.hitBox)){
                    shipsDestroyed ++;
                    scoreString = new String("Ships Destroyed: " + shipsDestroyed);
                    bullet.isDead = true;
                    deadBullets.add(bullet);
                    enemyShip.isDead = true;
                    deadShips.add(enemyShip);
                }
            }
        }
        activeBullets.removeAll(deadBullets);
    }

    private void spawnEnemyShips() {
        //reset the spawn timer
            lastEnemySpawnTime = TimeUtils.nanoTime(); //should probably be delta time
        //make a new enemy ship, setting it's spawn location to a random location at the top of the screen
            EnemyShip newEnemyShip = new EnemyShip(MathUtils.random(800-64), enemyShipTexture);
        //add the new object to our list of active enemy ships
            activeEnemyShips.add(newEnemyShip);
    }

    private void updateEnemyShips() {
        // update all ships in activeEnemyShips
        activeEnemyShips.forEach(x -> x.update(Gdx.graphics.getDeltaTime()));
        // remove dead ships from the activeEnemyShips arraylist
        activeEnemyShips.removeAll(deadShips);

    }

    @Override
    public void render(float delta) {
        // clear the screen with a dark blue color. The
        // arguments to glClearColor are the red, green
        // blue and alpha component in the range [0,1]
        // of the color to be used to clear the screen.
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // tell the camera to update its matrices.
        camera.update();

        // tell the SpriteBatch to render in the
        // coordinate system specified by the camera.
        game.batch.setProjectionMatrix(camera.combined);

        // begin a new batch and draw all the ships and bullets
        game.batch.begin();

        for (Bullet bullet : activeBullets){
            bullet.render(game.batch);
        }

        for (EnemyShip enemyShip : activeEnemyShips){
            enemyShip.render(game.batch);
        }
        game.font.draw(game.batch, scoreString, 0, 480);
        game.batch.draw(shipImage, ship.x, ship.y);
        game.batch.end();

        // process user input
        if (Gdx.input.isTouched()) {
            Vector3 touchPos = new Vector3();
            touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
            camera.unproject(touchPos);
            ship.x = touchPos.x - 64 / 2;
        }

        if (Gdx.input.isKeyPressed(Keys.LEFT))
            ship.x -= 200 * Gdx.graphics.getDeltaTime();
        if (Gdx.input.isKeyPressed(Keys.RIGHT))
            ship.x += 200 * Gdx.graphics.getDeltaTime();

        // make sure the ship stays within the screen bounds
        if (ship.x < 0)
            ship.x = 0;
        if (ship.x > 800 - 64)
            ship.x = 800 - 64;

        //move the enemy ships, remove any that are below the bottom edge of the screen and adjust any that spawned off screen edge
        if (TimeUtils.nanoTime() - lastEnemySpawnTime > 900000000) { //TODO extract the 900000000 into a variable
            spawnEnemyShips();
        }
        // spawn bullets
        if (TimeUtils.nanoTime() - lastBulletTime > 600000000) { //TODO extract the 600000000 into a variable
            spawnBullets();
        }

        updateBullets();

        updateEnemyShips();
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void show() {
        // start the playback of the background music
        // when the screen is shown
        backgroundMusic.play();
    }

    @Override
    public void hide() {
        backgroundMusic.pause();
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
        shipImage.dispose();
        bulletSound.dispose();
        backgroundMusic.dispose();
        enemyShipTexture.dispose();
        bulletTexture.dispose();
    }

}