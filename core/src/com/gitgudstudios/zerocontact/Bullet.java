package com.gitgudstudios.zerocontact;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public class Bullet {
    public static final int SPEED = 700;
    private static Texture bulletImage;
    Rectangle hitBox;
    float x, y;
    public Boolean isDead;

    public Bullet(float originX, float originY, Texture tex){
        this.x = originX;
        this.y = originY;
        this.bulletImage = tex;
        this.isDead = false;
        this.hitBox = new Rectangle(x, y, 16, 16);
    }
    public void update(float deltaTime){
        y += SPEED * deltaTime;
        hitBox.y = y;
        if (y > Gdx.graphics.getHeight()){
            isDead = true;
        }
    }
    public void render(SpriteBatch batch){
        if (!isDead){
            batch.draw(bulletImage, x, y);
        }
    }
}
